-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 05, 2017 at 05:41 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roomsoom_thecitylegend`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `user_id` int(10) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(100) NOT NULL,
  `age` int(100) UNSIGNED DEFAULT NULL,
  `mobile` varchar(10) NOT NULL,
  `address` varchar(300) NOT NULL,
  `gendor` varchar(100) DEFAULT NULL,
  `interest` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`user_id`, `user_name`, `email`, `password`, `age`, `mobile`, `address`, `gendor`, `interest`) VALUES
(0, 'admin', 'mukesh.arya24@gmail.com', 'admin', 40, '', '', 'male', '');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(100) NOT NULL,
  `course_id` int(100) NOT NULL,
  `ip_address` varchar(300) NOT NULL,
  `user_id` int(100) NOT NULL,
  `course_title` varchar(300) NOT NULL,
  `course_image` varchar(300) NOT NULL,
  `course_type` varchar(300) NOT NULL,
  `course_college` int(100) NOT NULL,
  `course_price` varchar(300) NOT NULL,
  `total_amount` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `course_id`, `ip_address`, `user_id`, `course_title`, `course_image`, `course_type`, `course_college`, `course_price`, `total_amount`) VALUES
(1, 2, '3.196.73.45', 1, 'Electrical Engineering', 'electrical.jpg', 'Weekend Course', 3, '2000', '2000'),
(4, 2, '3.196.73.45', 4, 'Aerospace Engineering', 'aeronautical.jpg', 'Weekend Course', 38, '2000', '2000'),
(5, 3, '3.196.73.45', 5, 'Civil Engineering', 'civil.jpg', 'Weekend Course', 33, '2000', '2000'),
(6, 2, '3.196.73.45', 5, 'Aerospace Engineering', 'aeronautical.jpg', 'Weekend Course', 38, '2000', '2000'),
(7, 5, '3.196.73.45', 5, 'Electrical Engineering', 'electrical.jpg', 'Weekend Course', 25, '2000', '2000'),
(8, 4, '3.196.73.45', 5, 'Computer Engineering', 'computer.jpg', 'Weekend Course', 29, '2000', '2000'),
(9, 1, '3.196.73.45', 6, 'Architecture Engineering', 'architecture.jpg', 'Weekend Course', 37, '2000', '2000'),
(10, 3, '3.196.73.45', 6, 'Civil Engineering', 'civil.jpg', 'Weekend Course', 33, '2000', '2000'),
(11, 2, '3.196.73.45', 6, 'Aerospace Engineering', 'aeronautical.jpg', 'Weekend Course', 38, '2000', '2000'),
(12, 1, '3.196.73.45', 4, 'Architecture Engineering', 'architecture.jpg', 'Weekend Course', 37, '2000', '2000'),
(13, 1, '3.196.73.45', 9, 'Architecture Engineering', 'architecture.jpg', 'Weekend Course', 37, '2000', '2000'),
(14, 2, '3.196.73.45', 9, 'Aerospace Engineering', 'aeronautical.jpg', 'Weekend Course', 38, '2000', '2000'),
(15, 3, '3.196.73.45', 9, 'Civil Engineering', 'civil.jpg', 'Weekend Course', 33, '2000', '2000'),
(16, 4, '3.196.73.45', 9, 'Computer Engineering', 'computer.jpg', 'Weekend Course', 29, '2000', '2000'),
(17, 1, '3.196.73.45', 10, 'Architecture Engineering', 'architecture.jpg', 'Weekend Course', 37, '2000', '2000'),
(18, 1, '3.196.73.45', 12, 'Architecture Engineering', 'architecture.jpg', 'Weekend Course', 37, '2000', '2000');

-- --------------------------------------------------------

--
-- Table structure for table `college_info`
--

CREATE TABLE `college_info` (
  `college_id` int(100) NOT NULL,
  `discipline_id` int(100) NOT NULL,
  `stream_id` int(100) NOT NULL,
  `college_name` text NOT NULL,
  `college_address1` text NOT NULL,
  `college_address2` text NOT NULL,
  `college_city` text NOT NULL,
  `college_state` text NOT NULL,
  `college_country` varchar(200) NOT NULL,
  `college_website` varchar(300) NOT NULL,
  `college_image` varchar(300) NOT NULL,
  `college_contact` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `college_info`
--

INSERT INTO `college_info` (`college_id`, `discipline_id`, `stream_id`, `college_name`, `college_address1`, `college_address2`, `college_city`, `college_state`, `college_country`, `college_website`, `college_image`, `college_contact`) VALUES
(1, 1, 1, 'National Institute of Design', 'Paldi  Ahmedabad 380 007\r\nPhone:+91 79 2662 9500 /2662 9600\r\nFax: +91 79 26621167', '', 'Ahmedabad', 'Gujarat', 'India', 'http://www.nid.edu/institute/campuses/ahmedabad.html', 'NIDlogo.jpg', ''),
(2, 1, 1, 'Industrial Design Centre (IDC)  IIT Bombay', 'Industrial Design Centre \r\nIndian Institute of Technology Bombay \r\nPowai Mumbai 400 076  \r\nIndia \r\nPhone\r\n091-022-2576 7801\r\nFax\r\n091-022-2576 7803\r\n091-022-2572 3480', '', 'Mumbai', 'Maharashtra', 'India', 'http://www.idc.iitb.ac.in/index.php', 'IDChome_logo.jpg', ''),
(3, 1, 1, 'Indian Institute of Digital Art & Animation', 'BJ-97  BJ Block Sector II Salt Lake City Kolkata West Bengal 700091', '', 'Kolkata', 'West Bengal', 'India', 'http://iidaaindia.com/', '', ''),
(4, 1, 1, 'Birla Institute of Technology\r\n', 'Campus Vidya Vihar Pilani Pilani Road BITS  Pilani Rajasthan 333031\r\nPhone:01596 245 073\r\n', '', 'Pilani\r\n', 'Rajasthan\r\n', 'India\r\n', 'http://www.bits-pilani.ac.in/\r\n', 'BITS_university_logo.jpg\r\n', ''),
(5, 1, 1, 'Film and Television Institute of India\r\n', '\"Law College Road Pune - 411 004 \r\n(Maharashtra) INDIA.\r\n\r\nTel No.: \r\n+91 - 020- 25431817 / 25433016 / 25430017\"\r\n', 'Pune\r\n', 'Pune\r\n', 'Maharashtra\r\n', 'India\r\n', 'http://www.ftiindia.com/\r\n', 'ftii.jpg\r\n', ''),
(6, 1, 1, 'Vogue Institute of Fashion Technology\r\n', '\"No. 4 Anand Towers III Floor Opp. To PF Building \r\nRajaram Mohan Roy Road  Richmond Circle\r\nBangalore ? 560025 Karnataka  India.\r\nGive a Missed Call: 02261816372\"\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.voguefashioninstitute.com/\r\n', 'vogue-logo.jpg\r\n', ''),
(7, 1, 1, 'IIFA Multimedia (Institute for Interior Fashion and Animation)\r\n', '\"Address: 261 80 feet main road 9th Main road Srinivasa Nagar near banckcolony bus stop Banashankari Bengaluru Karnataka 560050\r\nPhone: 098450 06824\"\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.iifamultimedia.in/index.php\r\n', 'IIFA_logo.jpg\r\n', ''),
(8, 1, 1, 'National Institute of Management Studies - Bangalore GOVERNMENT\r\n', '\"Address: 123 1st Main Road  Kengeri Satellite Town  Bengaluru  Karnataka 560060\r\nPhone: 080 2848 5569\"\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', '', 'nims_logo.jpg\r\n', ''),
(9, 1, 1, 'Manipal University Bangalore Campus (Department of Animation) GOVERNMENT\r\n', 'Manipal.edu  Madhav Nagar  Manipal-576104\r\n', '', 'Manipal\r\n', 'Karnataka\r\n', 'India\r\n', 'http://manipal.edu/\r\n', 'manipal_logo.jpg\r\n', ''),
(10, 1, 1, 'Arena Multimedia Bangalore\r\n', '\"11  11TH MAIN ROAD  \r\nJAYANAGAR 4TH BLOCK  OPPOSITE ARVIND STORES  BANGALORE  KARNATAKA\r\nINDIA-560011\r\nPh: 80-26653866 / 80-41307228 / 919341228440 / 918546811140 \"\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.arena-multimedia.com/\r\n', '', ''),
(11, 2, 6, 'Christ University (CU Bangalore)\r\n', 'Hosur Road  Bangalore  Karnataka, India- 560029\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.christuniversity.in/\r\n', 'ChristUniv-logo.jpg\r\n', ''),
(12, 2, 6, 'St. Joseph\'s College of Commerce (SJCC)\r\n', 'No:163  Brigade Road Bangalore  Karnataka  India- 560025\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.sjc.ac.in/\r\n', 'St Joseph-logo.jpg\r\n', ''),
(13, 2, 6, 'MOUNT CARMEL COLLEGE - [MCC]\r\n', 'No.58  Palace Road  Vasanth Nagar Bengaluru Karnataka 560052\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.mountcarmelcollegeblr.co.in/\r\n', 'Mount Carmel-logo.jpg\r\n', ''),
(14, 3, 16, 'R V College of Engineering\r\n', 'R V Vidyanikethan Post Mysore Road Bangalore Karnataka India- 560059         \r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.rvce.edu.in/\r\n', 'rvce-logo.jpg\r\n', ''),
(15, 3, 16, 'PES University\r\n', '100 Feet Ring Road BSK III Stage Banashankari Bangalore Karnataka India- 560085\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://pesit.pes.edu/\r\n', 'pesit-logo.jpg\r\n', ''),
(16, 3, 16, 'BMS College of Engineering\r\n', 'P.O. Box No. 1908 Bull Temple Road Kempe Gowda Road Bangalore Karnataka India- 560019\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.bmsce.ac.in/\r\n', 'bmsce-logo.jpg\r\n', ''),
(17, 3, 16, 'M S Ramaiya College of Engineering\r\n', 'MSR Nagar MSRIT Post 80 Ft. Road Bangalore Karnataka India- 560054\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.msrit.edu:8080/index.jsp\r\n', 'msrit-logo.jpg\r\n', ''),
(18, 3, 14, 'R V College of Engineering\r\n', 'R V Vidyanikethan Post Mysore Road Bangalore Karnataka India- 560059         \r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.rvce.edu.in/\r\n', 'rvce-logo.jpg\r\n', ''),
(19, 3, 14, 'PES University\r\n', '100 Feet Ring Road BSK III Stage Banashankari Bangalore Karnataka India- 560085\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://pesit.pes.edu/\r\n', 'pesit-logo.jpg\r\n', ''),
(20, 3, 14, 'BMS College of Engineering\r\n', 'P.O. Box No. 1908 Bull Temple Road Kempe Gowda Road Bangalore Karnataka India- 560019\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.bmsce.ac.in/\r\n', 'bmsce-logo.jpg\r\n', ''),
(21, 3, 14, 'M S Ramaiya College of Engineering\r\n', 'MSR Nagar MSRIT Post 80 Ft. Road Bangalore Karnataka India- 560054\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.msrit.edu:8080/index.jsp\r\n', 'msrit-logo.jpg\r\n', ''),
(22, 3, 13, 'R V College of Engineering\r\n', 'R V Vidyanikethan Post Mysore Road Bangalore Karnataka India- 560059         \r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.rvce.edu.in/\r\n', 'rvce-logo.jpg\r\n', ''),
(23, 3, 13, 'PES University\r\n', '100 Feet Ring Road BSK III Stage Banashankari Bangalore Karnataka India- 560085\r\n', '\r\n', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://pesit.pes.edu/\r\n', 'pesit-logo.jpg', ''),
(24, 3, 13, 'BMS College of Engineering\r\n', 'P.O. Box No. 1908 Bull Temple Road Kempe Gowda Road Bangalore Karnataka India- 560019\r\n', '', 'Bengaluru\r\n', 'Karnataka\r\n', 'India\r\n', 'http://www.bmsce.ac.in/\r\n', 'bmsce-logo.jpg\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `course_info`
--

CREATE TABLE `course_info` (
  `course_id` int(100) NOT NULL,
  `course_title` text NOT NULL,
  `course_college` int(100) NOT NULL,
  `course_discipline` int(100) NOT NULL,
  `course_stream` int(100) NOT NULL,
  `course_desc` text NOT NULL,
  `course_image` varchar(300) NOT NULL,
  `course_type` varchar(300) NOT NULL,
  `course_price` varchar(300) NOT NULL,
  `course_keywords` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_info`
--

INSERT INTO `course_info` (`course_id`, `course_title`, `course_college`, `course_discipline`, `course_stream`, `course_desc`, `course_image`, `course_type`, `course_price`, `course_keywords`) VALUES
(1, 'Architecture Engineering', 37, 3, 9, '', 'architecture.jpg', 'Weekend Course', '2000', 'architecture, msrit'),
(2, 'Aerospace Engineering', 38, 3, 10, '', 'aeronautical.jpg', 'Weekend Course', '2000', 'Aerospace Aeronautical'),
(3, 'Civil Engineering', 33, 3, 11, '', 'civil.jpg', 'Weekend Course', '2000', 'civil, msrit'),
(4, 'Computer Engineering', 29, 3, 12, '', 'computer.jpg', 'Weekend Course', '2000', 'computer'),
(5, 'Electrical Engineering', 25, 3, 13, '', 'electrical.jpg', 'Weekend Course', '2000', 'electrical');

-- --------------------------------------------------------

--
-- Table structure for table `discipline_info`
--

CREATE TABLE `discipline_info` (
  `discipline_id` int(100) NOT NULL,
  `discipline_title` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discipline_info`
--

INSERT INTO `discipline_info` (`discipline_id`, `discipline_title`) VALUES
(1, 'Arts'),
(2, 'Commerce'),
(3, 'Engineering'),
(4, 'Hospitality'),
(5, 'Law'),
(6, 'Medicine'),
(7, 'Sciences');

-- --------------------------------------------------------

--
-- Table structure for table `eventinterest`
--

CREATE TABLE `eventinterest` (
  `eventInterestId` mediumint(9) NOT NULL,
  `eventId` mediumint(8) UNSIGNED NOT NULL,
  `shopping` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `food` varchar(255) NOT NULL,
  `drink` varchar(255) NOT NULL,
  `park` varchar(255) NOT NULL,
  `music` varchar(255) NOT NULL,
  `beauty` varchar(255) NOT NULL,
  `fun` varchar(255) NOT NULL,
  `rideshare` varchar(255) NOT NULL,
  `business` varchar(255) NOT NULL,
  `learning` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventinterest`
--

INSERT INTO `eventinterest` (`eventInterestId`, `eventId`, `shopping`, `experience`, `food`, `drink`, `park`, `music`, `beauty`, `fun`, `rideshare`, `business`, `learning`) VALUES
(1, 1, 'shopping', '', 'food', '', 'park', '', '', '', 'rideshare', '', ''),
(2, 3, '', '', '', '', '', '', '', '', '', '', 'learning'),
(3, 4, '', '', '', '', '', '', 'beauty', '', '', '', 'learning'),
(4, 5, '', '', '', '', '', '', '', '', '', '', ''),
(5, 6, '', '', '', '', '', '', '', '', '', 'business', ''),
(6, 7, '', 'experience', 'food', '', '', '', '', '', '', '', ''),
(7, 8, 'Shopping', 'experience', '', 'drink', 'park', 'music', 'beauty', 'fun', 'rideshare', 'business', 'learning'),
(8, 9, 'Shopping', 'experience', '', 'drink', 'park', 'music', 'beauty', 'fun', 'rideshare', 'business', 'learning'),
(9, 10, 'shopping', 'experience', '', '', '', '', '', '', '', '', ''),
(10, 11, '', 'experience', 'food', 'drink', 'park', 'music', '', 'fun', '', 'business', ''),
(11, 12, 'shopping', '', '', '', '', 'music', '', 'fun', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `eventId` mediumint(9) NOT NULL,
  `eventManagerName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `eventphone` varchar(255) NOT NULL,
  `eventName` varchar(255) NOT NULL,
  `eventDate` date NOT NULL,
  `eventStarttime` time NOT NULL,
  `eventAddress` varchar(2048) NOT NULL,
  `eventCity` varchar(255) NOT NULL,
  `zip` mediumint(9) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `eventState` varchar(255) NOT NULL,
  `eventDetail` varchar(10000) NOT NULL,
  `eventOccurance` varchar(255) NOT NULL,
  `flyer` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`eventId`, `eventManagerName`, `email`, `eventphone`, `eventName`, `eventDate`, `eventStarttime`, `eventAddress`, `eventCity`, `zip`, `landmark`, `latitude`, `longitude`, `eventState`, `eventDetail`, `eventOccurance`, `flyer`, `video`, `status`) VALUES
(3, 'Mukesh', 'mukesh.arya24@gmail.com', '', 'PHP learning ', '2017-04-29', '00:00:00', 'REA 204 PURVA RIVIERA', 'BANGALORE', 560037, 'spice garden', 12.9582, 77.7078, 'Karnataka', 'Thanks', '', 'none', 'none', ''),
(4, 'Harsha', 'mukesh.arya24@gmail.com', '', 'rock show', '2017-04-14', '00:00:00', 'REA 204 PURVA RIVIERA', 'BANGALORE', 560037, 'Karnataka', 12.9582, 77.7078, 'Karnataka', '', '', '', '', ''),
(5, 'harsha', 'harsh.hn@gmail.com', '', 'potluck', '2017-04-28', '00:00:00', 'Hoodi', 'Bangalore', 560048, 'KARNATAKA', 12.9922, 77.7159, 'KARNATAKA', '', '', '', '', ''),
(6, 'Derika', 'info@madebyamaven.cm', '', 'City Legend Meeting', '2017-04-29', '00:00:00', '2640 Nolensville Rd', 'Nashville', 37211, 'TN', 36.1148, -86.7467, 'TN', '', '', '', '', ''),
(7, 'Flying Saucer Draught', 'marcus.rodgers@gmail.com', '', 'Pints Team Up with Pints for Prostates', '2017-05-01', '00:00:00', '111 10th Ave S, Ste 310, Nashville, Tennesee 37203', 'Nashville', 37209, 'TN', 36.1568, -86.784, 'TN', '', '', '', '', ''),
(8, 'Sanu', 'mukesh.arya24@gmail.com', '', 'SEO', '2017-05-26', '00:00:00', 'REA 204 PURVA RIVIERA', 'BANGALORE', 560037, 'Karnataka', 0, 0, 'Karnataka', '', '', '', '', 'apporved'),
(9, 'sanu', 'mukesh.arya24@gmail.com', '', 'SEO ', '2017-05-26', '00:00:00', 'REA 204 PURVA RIVIERA', 'BANGALORE', 560037, 'Karnataka', 0, 0, 'Karnataka', '', '', '', '', 'apporved'),
(10, 'Kishan Vemula', 'kishanvemula@gmail.com', '', 'fun time with friend', '2017-05-11', '00:59:00', 'REA 204 PURVA RIVIERA', 'BANGALORE', 560037, 'Karnataka', 12.9582, 77.7078, 'Karnataka', 'fun time ', 'monthly', '', '', 'approved'),
(11, 'Derika', 'derikafeatherston@gmail.com', '9104205525', 'Test Event', '2017-05-17', '19:00:00', '2901 Mavert Drive', 'Nashville', 37211, 'Kroger', 36.1083, -86.7279, 'TN', 'COME AS YOU ARE 11111112222222233333333444444444555676545830458-3948524985!@#$%^*()_+_)(*&^%$#@!~!@#$%^&*()_+_)(*!QWERTGHJKOP{}{:LKJHYTRESXDCFVGBHNJKL:\"?><MNBVCXZSW#E$RTGFE$%TYUJNHGYUIKL<MNHYUIOP:><MKJIOP{:><MJHYGTREMODIFKGGHFPFIJ;KHAG68E89P9ROIAIDFAB N E     PQOIEHRPEURYGYFHJVKP989AD7FGYVBNMEK[QIOURHEIJLNA;DMFJAI9F876T2FY3VGBHNJK0[A9DG8765RDCFGVHBNJIOKOUHJNKIO0-P=][32E[P-O0PE9IRUHJ1KOP[FAPLD,PGMONAIDBFGHA\r\n\r\nASODIFUAYSGDF78YP9UEIU87YFGUBHNCMAODKFNASDKFNDMNALB E6879810-3I9 H   0IJHJZHY1`HP931U849GY73THU  JDKFNADBHFGAUYSDF8U09AIDPOKJNTH', 'daily', '', '', 'approved'),
(12, 'Marcus', 'juntao85@hotmail.com', '615 779 3027', 'Although', '2017-05-24', '02:00:00', '107 Rifle Range Rd', 'Hermitage', 37138, 'Homes', 36.2385, -86.6372, 'TN', 'watch out', 'daily', '', '', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `stream_info`
--

CREATE TABLE `stream_info` (
  `stream_id` int(100) NOT NULL,
  `discipline_id` int(100) NOT NULL,
  `stream_title` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stream_info`
--

INSERT INTO `stream_info` (`stream_id`, `discipline_id`, `stream_title`) VALUES
(1, 1, 'Animation'),
(2, 1, 'Designing'),
(3, 1, 'Film/Media'),
(4, 1, 'Journalism'),
(5, 1, 'Languages'),
(6, 2, 'Accounting'),
(7, 2, 'Banking'),
(8, 2, 'Finance'),
(9, 3, 'Architecture'),
(10, 3, 'Aerospace'),
(11, 3, 'Civil'),
(12, 3, 'Computer'),
(13, 3, 'Electrical'),
(14, 3, 'Electronics'),
(15, 3, 'Information Sciences'),
(16, 3, 'Mechanical'),
(17, 3, 'Marine'),
(18, 4, 'Chef'),
(19, 4, 'Event Management'),
(20, 4, 'Hotel Management'),
(21, 5, 'Agriculture'),
(22, 5, 'Dental'),
(23, 5, 'Medical'),
(24, 5, 'Nursing'),
(25, 5, 'Pharmacy'),
(26, 5, 'Physiotherapy'),
(27, 6, 'BCA'),
(28, 6, 'Biology'),
(29, 6, 'Biotechnology'),
(30, 6, 'Chemistry'),
(31, 6, 'Environmental Sciences'),
(32, 6, 'Mathematics'),
(33, 6, 'Physics'),
(34, 6, 'BBA'),
(35, 6, 'MBA');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(10) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(100) NOT NULL,
  `age` mediumint(8) UNSIGNED DEFAULT NULL,
  `mobile` varchar(100) NOT NULL,
  `address` varchar(300) NOT NULL,
  `gendor` varchar(100) DEFAULT NULL,
  `interest` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `user_name`, `email`, `password`, `age`, `mobile`, `address`, `gendor`, `interest`) VALUES
(16, 'mukesh', 'mukesh.arya24@gmail.com', 'something', 40, '09886134624', '', 'male', ''),
(17, 'sanu', 'virtualtaskhub@gmail.com', 'something', 30, '9876346735', '', 'male', ''),
(18, 'derika', 'derika@madebyamaven.com', 'something', 20, '5677899856', '', 'male', ''),
(19, 'sanu', 'helpdesk@virtualtaskhub.com', 'something', 40, '9886934624', '', 'male', ''),
(20, 'kabir', 'kabir@gmail.com', 'something', 20, '9886934624', '', 'male', ''),
(21, 'Maven', 'info@madebyamaven.com', 'sheila20', 31, '', '', 'male', ''),
(22, 'Pettydd', 'derikaf@me.com', 'sheila20', 31, '', '', 'male', ''),
(23, 'Noexcuses82', 'marcus.rodgers@gmail.com', 'Kingbbf3', 35, '6157793027', '', 'male', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `college_info`
--
ALTER TABLE `college_info`
  ADD PRIMARY KEY (`college_id`);

--
-- Indexes for table `course_info`
--
ALTER TABLE `course_info`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `discipline_info`
--
ALTER TABLE `discipline_info`
  ADD PRIMARY KEY (`discipline_id`);

--
-- Indexes for table `eventinterest`
--
ALTER TABLE `eventinterest`
  ADD PRIMARY KEY (`eventInterestId`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`eventId`);

--
-- Indexes for table `stream_info`
--
ALTER TABLE `stream_info`
  ADD PRIMARY KEY (`stream_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `college_info`
--
ALTER TABLE `college_info`
  MODIFY `college_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `course_info`
--
ALTER TABLE `course_info`
  MODIFY `course_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `discipline_info`
--
ALTER TABLE `discipline_info`
  MODIFY `discipline_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `eventinterest`
--
ALTER TABLE `eventinterest`
  MODIFY `eventInterestId` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `eventId` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `stream_info`
--
ALTER TABLE `stream_info`
  MODIFY `stream_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
