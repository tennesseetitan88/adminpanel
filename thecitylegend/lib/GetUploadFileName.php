<?php
class GetUploadFileName
{
    // property declaration
	private $fName = '';
	private $originalName  = '';

	public function GetUploadFileName($fileName) {
	 	$this->originalName  = $fileName;	
		$this->fName = $fileName;
		$this->fName = preg_replace('/\s+/', '', $this->fName);
    		$appended = date('_m_d_Y');
    		$dotCount = substr_count($this->fName, '.');
    		if (!$dotCount) {
			$fName = $this->fName . $appended;
    		} else {
    			$extension = pathinfo($this->fName, PATHINFO_EXTENSION);
    			$fileName = pathinfo($this->fName, PATHINFO_FILENAME);
			$this->fName = $fileName . $appended . '.' . $extension;
		}
	}

	public function getFileName() {
		return $this->fName; 
	} 

	public function getOriginalFileName() {
		return $this->originalName; 
	} 

}
?>
