<?php
    include('db.php');
//echo 'event id :' . $_GET['eventId'];
                       // $result = select * from  'events' where 'eventId'='$_GET['eventId']'");
                        $select_query = "SELECT * FROM events where eventId = '".$_GET["eventId"]."'";
                       $result = mysqli_query($con,$select_query);
$row = mysqli_fetch_assoc($result);
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>SmartForest - Dashboard</title>

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Material Design Bootstrap -->
<link href="css/main.css" rel="stylesheet">

<!-- Your custom styles (optional) -->
<link href="css/dashboard.css" rel="stylesheet">



</head>

<body>

	<nav class="navbar navbar-dark green superadminnav">
		<button class="navbar-toggler hidden-sm-up" type="button"
			data-toggle="collapse" data-target="#collapseEx2">
			<i class="fa fa-bars"></i>
		</button>
		<div class="container-fluid">
			<div class="collapse navbar-toggleable-xs" id="collapseEx2">

				<a href="#" class="navbar-brand"> The City Legend</a>
				<ul class="nav navbar-nav float-xs-right">

					<li class="nav-item alertitem" style="display: none;"><a
						class="nav-link waves-effect waves-light btn btn-danger animated bounce infinite"><i
							class="fa fa-envelope"></i></a></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle waves-effect waves-light"
						type="button" id="dropdownMenu1" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"><i
							class="fa fa-user"></i> My Account</a>
						<div class="dropdown-menu dropdown-primary dd-right"
							aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn"
							data-dropdown-out="fadeOut">
							<a class="dropdown-item waves-effect waves-light logout" href="#">Logout</a>

						</div></li>
				</ul>
			</div>
		</div>
	</nav>

	<!-- end of navbar -->
	<div id="wrapper">
     <div id="sidebar-wrapper">
     <section class="sidebar" style="">
   
     
            <ul class="sidebar-menu">
                <li class="treeview  addadmin"><a href="superadmindashboard.php"> Add Activity</a></li>
                <li class="home dashboard treeview active"><a href="viewadmindetails.php">Manage Activity</a></li>
                <li class="home dashboard treeview"><a href="viewuserdetails.php">View User</a></li>
            </ul>
            </section>
        </div>
            <!-- /.sidebar -->
            <!-- dashboard Page Content -->
    <div id="page-content-wrapper">
    
   
    <div class="sound"></div>
    <div class="content-wrapper dashboardsidebar sidebarcontent" id="dashboard" style=" overflow:auto;">
        <div class="container-fluid">
        <div class="col-md-12">
            <div class="card" style="margin-top: 20px;">
                <form id="new-activity" action="editExistingEvent.php" method="post" enctype= "multipart/form-data">
                    <div class="card-block">
                    <div class="form-header brown darken-4">
                        <h3>Edit Event : <?php echo $row['eventName'] ?> : hosted by : <?php echo $row['eventManagerName'] ?> </h3>
                    </div>
                     <input type="hidden" id="act_eventid" class="form-control" name="act_eventid" value="<?php echo $row['eventId']?>">
                    <div class="md-form">
                        <input type="text" id="act_username" class="form-control" name="act_username" value="<?php echo $row['eventManagerName']?>" required>
                        <label for="act_username">Event Manager </label>
                    </div>
                    <div class="md-form">
                        <input type="email" id="act_email" class="form-control" name="act_email" value="<?php echo $row['email']?>" required>
                        <label for="act_email">Email :</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_phonenumber" class="form-control" name="act_phonenumber" value="<?php echo $row['eventphone']?>">
                        <label for="act_phonenumber">Event Phone Number</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventname" class="form-control" name="act_eventname" value="<?php echo $row['eventName']?>" required>
                        <label for="act_eventname">Event Name</label>
                    </div>
                    <div class="md-form">
                        <input type="date" id="act_eventdate" class="form-control" name="act_eventdate"  value="<?php echo $row['eventDate']?>" required>
                        <label for="act_eventdate"></label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventaddress" class="form-control" name="act_eventaddress" value="<?php echo $row['eventAddress']?>" required>
                        <label for="act_eventaddress">Event Address</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventcity" class="form-control" name="act_eventcity" value="<?php echo $row['eventCity']?>" required>
                        <label for="act_eventcity">Event City</label>
                    </div>
                    <div class="md-form">
                        <input type="number" id="act_pincode" minlength="5" maxlength="10" class="form-control" name="act_pincode" value="<?php echo $row['zip']?>" required >
                        <label for="act_pincode">zip</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_landmark" class="form-control" name="act_landmark" value="<?php echo $row['landmark']?>">
                        <label for="act_eventlandmark">Land Mark</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventstate" class="form-control" name="act_eventstate" value="<?php echo $row['eventState']?>" required>
                        <label for="act_eventstate">State</label>
                    </div>
                    <div class="md-form">
                        <textarea type="text" id="act_eventdetail" class="form-control" name="act_eventdetail"><?php echo $row['eventDetail']?></textarea>
                        <label for="act_eventdetail">Event Detail</label>
                    </div>
                    <div class="md-form">
                        <input type="time" id="act_eventstarttime" class="form-control" name="act_eventstarttime" value="<?php echo $row['eventStarttime']?>" required/>
                        <label for="act_eventstarttime"></label>
                    </div>
                    <div class="md-form">
                        <select type="text" id="act_eventoccurance" class="form-control" name="act_eventoccurance" required >
                            <option value="onetime" <?php if ($row['eventOccurance'] == 'onetime') echo 'selected';?> >One Time</option>
                            <option value="daily" <?php if ($row['eventOccurance'] == 'daily') echo 'selected';?> >Daily</option>
                            <option value="weekly" <?php if ($row['eventOccurance'] == 'weekly') echo 'selected';?> >Weekly</option>
                            <option value="monthly" <?php if ($row['eventOccurance'] == 'monthly') echo 'selected';?>>Monthly</option>
                        </select>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_latitude" class="form-control" name="act_latitude" value="<?php echo $row['latitude']?>" required/>
                        <label for="act_eventdetail">Event Latitude</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_longitude" class="form-control" name="act_longitude" value="<?php echo $row['longitude']?>" required/>
                        <label for="act_eventdetail">Event Longitude</label>
                    </div>
                    <div class="md-form">
                        <b> Categories : </b>
                        <input type="radio" id="act_shopping" value="shopping" name="eventinterest" <?php echo (strtolower($row['eventinterest'])=='shopping')?'checked':'' ?> required >
                        <label for="act_shopping">Shopping</label> &nbsp; &nbsp;
                        <input type="radio" id="act_experience" value="experience" name="eventinterest" <?php echo ($row['eventinterest']=='experience')?'checked':'' ?> required >
                        <label for="act_experience">Experience</label> &nbsp; &nbsp;
                        <input type="radio" id="act_food" value="food" name="eventinterest" <?php echo ($row['eventinterest']=='food')?'checked':'' ?> required>
                        <label for="act_food">Food</label> &nbsp; &nbsp;
                        <input type="radio" id="act_drink" value="drink" name="eventinterest" <?php echo ($row['eventinterest']=='drink')?'checked':'' ?> required>
                        <label for="act_drink">Drink</label> &nbsp; &nbsp;
                        <input type="radio" id="act_park" value="park" name="eventinterest" <?php echo ($row['eventinterest']=='park')?'checked':'' ?> required>
                        <label for="act_park">Park</label> &nbsp; &nbsp;
                        <input type="radio" id="act_music" value="music" name="eventinterest" <?php echo ($row['eventinterest']=='music')?'checked':'' ?> required>
                        <label for="act_music">Music</label> &nbsp; &nbsp;
                        <input type="radio" id="act_beauty" value="beauty" name="eventinterest" <?php echo ($row['eventinterest']=='beauty')?'checked':'' ?> required>
                        <label for="act_beauty">Beauty</label> &nbsp; &nbsp;
                        <input type="radio" id="act_fun" value="fun" name="eventinterest" <?php echo ($row['eventinterest']=='fun')?'checked':'' ?> required>
                        <label for="act_fun">Fun</label> &nbsp; &nbsp;
                        <input type="radio" id="act_rideshare" value="rideshare" name="eventinterest" <?php echo ($row['eventinterest']=='rideshare')?'checked':'' ?> required>
                        <label for="act_rideshare">Rideshare</label> &nbsp; &nbsp;
                        <input type="radio" id="act_business" value="business" name="eventinterest" <?php echo ($row['eventinterest']=='business')?'checked':'' ?> required>
                        <label for="act_business">Business</label> &nbsp; &nbsp;
                        <input type="radio" id="act_learning" value="lesson" name="eventinterest" <?php echo ($row['eventinterest']=='lesson')?'checked':'' ?> required>
                        <label for="act_learning">Lesson</label> &nbsp; &nbsp;
                    </div>
                    <hr />
                    <div class="flex-center">
                    <div class="col-md-4">
                              Flyer picture in record: <img src="<?php echo $row['flyer']?>">
                         <button class="btn btn-primary"> 
                             Upload Flyer <input type="file" name="flyer">
                        <input type="hidden" id="act_flyer" name="act_flyer" value="<?php echo $row['flyer'] ?>" >
                        </button>
                    </div>
                    <div class="col-md-4">
                        <p class="btn btn-primary btn-large" onclick="calculateLatLng();"> calculate latitude longitude </p>
                    </div>
                    </div>

                    <div class="flex-center">
                        <p id="responseStr"></p>
                    </div>
                    <!--Blue select-->

                    <div class="text-xs-center">
                        <input placeholder="Submit" class="btn btn-large btn-primary"type="submit" id="act_formsubmit">

                        <hr>

                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>

	<!-- JQuery -->


	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type ="text/javascript" src="js/underscore-min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>>


	<script>
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
		$("#new-activity").submit (function() {
		    $("#new-activity").validate();
		});

	</script>
    <script src="js/GeoCoding.js"></script>
    <script src="js/activitiesUtil.js"></script>
    <script src="js/app.min.js"></script>



</body>

</html>

