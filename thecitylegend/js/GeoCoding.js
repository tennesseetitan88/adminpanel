/**
 * Created by mukes on 4/27/2017.
 */

function calculateLatLng() {
    address = document.getElementById("act_eventaddress").value;
    city = document.getElementById("act_eventcity").value;
    state = document.getElementById("act_eventstate").value;

   if( address != '' && city != '' && state != '') {
    	addressurl = getURL (address,city,state);

    $.ajax({
        url: addressurl,
        success: function(response) {
            if(response.results.length > 0) {
                document.getElementById('act_latitude').value =  response.results[0].geometry.location.lat;
                document.getElementById('act_longitude').value = response.results[0].geometry.location.lng;
            }
        },
        error : function() {
          alert('could not find the lat lng, please calculate manually and submit')
        }
    })

   } else {
         alert('please complete address, city and state field to calculate lat lng');
   }
	
}

function getURL(address,city,state) {
    address = address.split(' ').join('+');

    //address=purva+riviera+marathahalli,+bangalore,+karnataka&key=AIzaSyDidEDu_o-E7DS0AkA83dOAefjprHDt-ok

    //https://maps.googleapis.com/maps/api/geocode/json?address=purva+riviera+marathahalli,+bangalore,+karnataka&key=AIzaSyDidEDu_o-E7DS0AkA83dOAefjprHDt-ok
    key = 'AIzaSyDidEDu_o-E7DS0AkA83dOAefjprHDt-ok';

    localurl = 'https://maps.googleapis.com/maps/api/geocode/json?' + 'address=' + address +',+' +  city + ',+' + state
            + '&key=' + key;

    return localurl;
}
