<? session_start();?> 

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>TheCityLegend</title>

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Material Design Bootstrap -->
<link href="css/main.css" rel="stylesheet">

<!-- Your custom styles (optional) -->
<link href="css/dashboard.css" rel="stylesheet">



</head>

<body>

	<nav class="navbar navbar-dark green superadminnav">
        <button class="navbar-toggler hidden-sm-up" type="button"
            data-toggle="collapse" data-target="#collapseEx2">
            <i class="fa fa-bars"></i>
        </button>
        <div class="container-fluid">
            <div class="collapse navbar-toggleable-xs" id="collapseEx2">

                <a href="#" class="navbar-brand"> The City Legend</a>
                <ul class="nav navbar-nav float-xs-right">

                    <ltitui class="nav-item alertitem" style="display: none;"><a
                        class="nav-link waves-effect waves-light btn btn-danger animated bounce infinite"><i
                            class="fa fa-envelope"></i></a></li>
                    <li class="nav-item dropdown"><a
                        class="nav-link dropdown-toggle waves-effect waves-light"
                        type="button" id="dropdownMenu1" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i
                            class="fa fa-user"></i> My Account</a>
                        <div class="dropdown-menu dropdown-primary dd-right"
                            aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn"
                            data-dropdown-out="fadeOut">
                            <a class="dropdown-item waves-effect waves-light logout" href="index.php">Logout</a>

                        </div></li>
                </ul>
            </div>
        </div>
    </nav>

	<div id="wrapper">
     <div id="sidebar-wrapper">
     <section class="sidebar" style="">
   
     
            <ul class="sidebar-menu">
                <li class="home dashboard treeview active"><a href="">Add Activity</a></li>
                <li class="home dashboard treeview "><a href="viewadmindetails.php">Manage Activity</a></li>
                <li class="home dashboard treeview"><a href="viewuserdetails.php">View User</a></li>
            </ul>
            </section>
        </div>
            <!-- /.sidebar -->
            <!-- dashboard Page Content -->
    <div id="page-content-wrapper">
    
   
    <div class="sound"></div>
    <div class="content-wrapper dashboardsidebar sidebarcontent" id="dashboard" style=" overflow:auto;">
        <div class="container-fluid">
        <div class="col-md-12">
            <div class="card" style="margin-top: 20px;">
                <form id="new-activity" action="enterNewEvent.php" method="post" enctype= "multipart/form-data">
                    <div class="card-block">
                    <div class="form-header brown darken-4">
                        <h3>Add An Activity</h3>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_username" class="form-control" name="act_username" required>
                        <label for="act_username">Event Manager </label>
                    </div>
                    <div class="md-form">
                        <input type="email" id="act_email" class="form-control" name="act_email" required>
                        <label for="act_email">Email :</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_phonenumber" class="form-control" name="act_phonenumber">
                        <label for="act_phonenumber">Event Phone Number</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventname" class="form-control" name="act_eventname" required>
                        <label for="act_eventname">Event Name</label>
                    </div>
                    <div class="md-form">
                        <input type="date" id="act_eventdate" class="form-control" name="act_eventdate" required>
                        <label for="act_eventdate" style="margin-left:50%"> Event Date</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventaddress" class="form-control" name="act_eventaddress" required>
                        <label for="act_eventaddress">Event Address</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventcity" class="form-control" name="act_eventcity" required>
                        <label for="act_eventcity">Event City</label>
                    </div>
                    <div class="md-form">
                        <input type="number" id="act_pincode" min="9999" max="9999999999" class="form-control" name="act_pincode" required >
                        <label for="act_pincode">zip</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventlandmark" class="form-control" name="act_eventlandmark">
                        <label for="act_eventlandmark">Land Mark</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_eventstate" class="form-control" name="act_eventstate" required />
                        <label for="act_eventstate">State</label>
                    </div>
                    <div class="md-form">
                        <textarea type="text" id="act_eventdetail" class="form-control" name="act_eventdetail" ></textarea>
                        <label for="act_eventdetail">Event Detail</label>
                    </div>
                    <div class="md-form">
                        <input type="time" id="act_eventstarttime" class="form-control" name="act_eventstarttime" required/>
                        <label for="act_eventstarttime" style="margin-left:60%">Event Start time</label>
                    </div>
                    <div class="md-form">
                        <label for="act_eventoccurance" style="margin-left:60%"> Event Occurance</label>
                        <select type="text" id="act_eventoccurance" class="form-control" name="act_eventoccurance" required >
                            <option value="OneTime">OneTime</option>
                            <option value="daily">Daily</option>
                            <option value="weekly">Weekly</option>
                            <option value="by-weekly">By-weekly</option>
                            <option value="monthly" selected>Monthly</option>
                            <option value="yearly">Yearly</option>
                        </select>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_latitude" class="form-control" name="act_latitude" required/>
                        <label for="act_latitude" style="margin-left:60%">Event Latitude</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_longitude" class="form-control" name="act_longitude" required/>
                        <label for="act_longitude" style="margin-left:60%">Event Longitude</label>
                    </div>
                    <div class="flex-center">
                        <p id="responseStr"></p>
                    </div>
                    <div class="md-form">
                        <b> Categories : </b>
                        <input type="radio" id="act_shopping" value="shopping" name="eventinterest">
                        <label for="act_shopping">shopping</label> &nbsp; &nbsp;
                        <input type="radio" id="act_experience" value="experience" name="eventinterest">
                        <label for="act_experience">Experience</label> &nbsp; &nbsp;
                        <input type="radio" id="act_food" value="food" name="eventinterest" >
                        <label for="act_food">Food</label> &nbsp; &nbsp;
                        <input type="radio" id="act_drink" value="drink" name="eventinterest" >
                        <label for="act_drink">Drink</label> &nbsp; &nbsp;
                        <input type="radio" id="act_park" value="park" name="eventinterest" >
                        <label for="act_park">Park</label> &nbsp; &nbsp;
                        <input type="radio" id="act_music" value="music" name="eventinterest" >
                        <label for="act_music">Music</label> &nbsp; &nbsp;
                        <input type="radio" id="act_beauty" value="beauty" name="eventinterest" >
                        <label for="act_beauty">Beauty</label> &nbsp; &nbsp;
                        <input type="radio" id="act_fun" value="fun" name="eventinterest" >
                        <label for="act_fun">Fun</label> &nbsp; &nbsp;
                        <input type="radio" id="act_rideshare" value="rideshare" name="eventinterest">
                        <label for="act_rideshare">Rideshare</label> &nbsp; &nbsp;
                        <input type="radio" id="act_business" value="business" name="eventinterest" >
                        <label for="act_business">Business</label> &nbsp; &nbsp;
                        <input type="radio" id="act_learning" value="learning" name="eventinterest" >
                        <label for="act_learning">Learning</label> &nbsp; &nbsp;
                    </div>
                    <hr />
                    <div class="flex-center">
                        <button role="button" type=button" class="btn btn-primary btn-large" >
                             Upload Flyer <input type="file" name="flyer" />
                        </button>
                        <button role="button" type=button" class="btn btn-success btn-large"  onclick="calculateLatLng();"> calculate latitude longitude </button>
                    </div>

                    <!--Blue select-->

                    <div class="text-xs-center">
                        <input placeholder="Submit" class="btn btn-large btn-primary" type="submit" id="act_formsubmit">

                        <hr>

                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

	<!-- JQuery -->


	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type ="text/javascript" src="js/underscore-min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>>


	<script>
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
		$("#new-activity").submit (function() {
		    $("#new-activity").validate();
		});

	</script>
    <script src="js/GeoCoding.js"></script>
    <script src="js/activitiesUtil.js"></script>
    <script src="js/app.min.js"></script>


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<!--Content-->
			<div class="modal-content">
				<!--Header-->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
			</div>
		</div>
	</div>
	<!-- /.Live preview-->
</body>

</html>
