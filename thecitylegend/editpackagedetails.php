<?php
    include('db.php');
//echo 'event id :' . $_GET['eventId'];
                       // $result = select * from  'events' where 'eventId'='$_GET['eventId']'");
                        $select_query = "SELECT * FROM package_info where pid = '".$_GET["pid"]."'";
                       $result = mysqli_query($con,$select_query);
$row = mysqli_fetch_assoc($result);
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>SmartForest - Dashboard</title>

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Material Design Bootstrap -->
<link href="css/main.css" rel="stylesheet">

<!-- Your custom styles (optional) -->
<link href="css/dashboard.css" rel="stylesheet">



</head>

<body>

	<nav class="navbar navbar-dark green superadminnav">
		<button class="navbar-toggler hidden-sm-up" type="button"
			data-toggle="collapse" data-target="#collapseEx2">
			<i class="fa fa-bars"></i>
		</button>
		<div class="container-fluid">
			<div class="collapse navbar-toggleable-xs" id="collapseEx2">

				<a href="#" class="navbar-brand"> The City Legend</a>
				<ul class="nav navbar-nav float-xs-right">

					<li class="nav-item alertitem" style="display: none;"><a
						class="nav-link waves-effect waves-light btn btn-danger animated bounce infinite"><i
							class="fa fa-envelope"></i></a></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle waves-effect waves-light"
						type="button" id="dropdownMenu1" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"><i
							class="fa fa-user"></i> My Account</a>
						<div class="dropdown-menu dropdown-primary dd-right"
							aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn"
							data-dropdown-out="fadeOut">
							<a class="dropdown-item waves-effect waves-light logout" href="#">Logout</a>

						</div></li>
				</ul>
			</div>
		</div>
	</nav>

	<!-- end of navbar -->
	<div id="wrapper">
     <div id="sidebar-wrapper">
     <section class="sidebar" style="">
   
     
            <ul class="sidebar-menu">
                <li class="treeview  addadmin"><a href="superadmindashboard.php"> Add Activity</a></li>
                <li class="home dashboard treeview active"><a href="viewadmindetails.php">Manage Activity</a></li>
                <li class="home dashboard treeview"><a href="viewuserdetails.php">View User</a></li>
                <li class="home dashboard treeview"><a href="packagecreation.php">Create Package</a></li>
                  <li class="home dashboard treeview"><a href="viewpackagedetails.php">View Package</a></li>
            </ul>
            </section>
        </div>
            <!-- /.sidebar -->
            <!-- dashboard Page Content -->
    <div id="page-content-wrapper">
    
   
    <div class="sound"></div>
    <div class="content-wrapper dashboardsidebar sidebarcontent" id="dashboard" style=" overflow:auto;">
        <div class="container-fluid">
        <div class="col-md-12">
            <div class="card" style="margin-top: 20px;">
                <form id="new-activity" action="editExistingpackage.php" method="post" enctype= "multipart/form-data">
                    <div class="card-block">
                    <div class="form-header brown darken-4">
                        <h3>Edit Package : <?php echo $row['packagename']; ?> </h3>
                    </div>
                     <input type="hidden" id="pid" class="form-control" name="pid" value="<?php echo $row['pid']?>">
                     <div class="md-form">
                        <input type="text" id="packagename" class="form-control" name="packagename" value="<?php echo $row['packagename']?>" required>
                        <label for="act_username">Package Name </label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="montlycost" class="form-control" name="montlycost" value="<?php echo $row['montlycost']?>" required>
                        <label for="montlycost">Monthly Cost </label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="totalpost" class="form-control" name="totalpost" value="<?php echo $row['totalposts']?>" required>
                        <label for="totalpost">Number of Posts </label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="totalimage" class="form-control" name="totalimage" value="<?php echo $row['totalimages']?>">
                        <label for="totalimage">Number of Images</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="catagery" class="form-control" name="catagery" value="<?php echo $row['categories']?>" required>
                        <label for="catagery">Number of Categories</label>
                    </div>
                    <div class="text-xs-center">
                        <input placeholder="Submit" class="btn btn-large btn-primary"type="submit" id="act_formsubmit">

                        <hr>

                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>

	<!-- JQuery -->


	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type ="text/javascript" src="js/underscore-min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>>


	<script>
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
		$("#new-activity").submit (function() {
		    $("#new-activity").validate();
		});

	</script>
    <script src="js/GeoCoding.js"></script>
    <script src="js/activitiesUtil.js"></script>
    <script src="js/app.min.js"></script>



</body>

</html>

