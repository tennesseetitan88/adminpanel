<?php
/**
 * Created by PhpStorm.
 * User: mukes
 * Date: 4/28/2017
 * Time: 2:39 PM
 */
include('db.php');
$lat = $_POST['lat'];
$lng = $_POST['lng'];
$distance = $_POST['distance'];
$container = array();
$counter = 0;
$response = array("status"=>"fail","activity"=>"");

$eventQuery = "SELECT *,(((acos(sin(($lat * pi()/180)) * sin((`latitude`*pi()/180))+cos(($lat * pi()/180))* cos((`latitude`*pi()/180)) * cos((($lng -`longitude`)*pi()/180))))*180/pi())*60*1.1515) AS `distance` FROM `events` HAVING `distance` <= $distance ORDER BY `distance` ASC";

$result = mysqli_query($con,$eventQuery);

if(is_bool($result)) {
   $response["status"] = "fail";
   $response["msg"] = mysqli_error();
} else {
    while ($row = mysqli_fetch_assoc($result)) {
        $item = array();
        foreach($row as $key => $val) {
            $item[$key] = $val;
        }
        $container[$counter] = $item;
        $counter = $counter + 1;
    }

    $response["status"] = "success";
    $response["activity"] = $container;

}

$myRes = json_encode($response);
echo $myRes;

?>
