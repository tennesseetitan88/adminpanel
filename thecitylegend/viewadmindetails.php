<?php
include_once ('db.php');
$sql = "SELECT * FROM events ORDER BY eventId DESC";
	$result = mysqli_query($con,$sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>TheCityLegend Dashboard</title>

<!-- Font Awesome -->
<link rel="stylesheet"	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/font-awesome-animation.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
    <!-- Styles -->
<body>
    <!-- end of navbar -->
     <nav class="navbar navbar-dark green superadminnav">
		<button class="navbar-toggler hidden-sm-up" type="button"
			data-toggle="collapse" data-target="#collapseEx2">
			<i class="fa fa-bars"></i>
		</button>
		<div class="container-fluid">
			<div class="collapse navbar-toggleable-xs" id="collapseEx2">

				<a href="#" class="navbar-brand"> The City Legend</a>
				<ul class="nav navbar-nav float-xs-right">

					<li class="nav-item alertitem" style="display: none;"><a
						class="nav-link waves-effect waves-light btn btn-danger animated bounce infinite"><i
							class="fa fa-envelope"></i></a></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle waves-effect waves-light"
						type="button" id="dropdownMenu1" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"><i
							class="fa fa-user"></i> My Account</a>
						<div class="dropdown-menu dropdown-primary dd-right"
							aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn"
							data-dropdown-out="fadeOut">
							<a class="dropdown-item waves-effect waves-light logout" href="#">Logout</a>

						</div></li>
				</ul>
			</div>
		</div>
	</nav>

    <div id="wrapper">
     <div id="sidebar-wrapper">
     <section class="sidebar" style="">
   
     
            <ul class="sidebar-menu">
                <li class="home dashboard treeview "><a href="superadmindashboard.php">Add Activity</a></li>
                <li class="home dashboard treeview active"><a href="viewadmindetails.php">Manage Activity</a></li>
                <li class="home dashboard treeview"><a href="viewuserdetails.php">View User</a></li>
                <li class="home dashboard treeview"><a href="configureAdmin.php">Configure Admin</a></li>
                 <li class="home dashboard treeview "><a href="packagecreation.php">Create Package</a></li>
                 <li class="home dashboard treeview"><a href="viewpackagedetails.php">View Package</a></li>
            </ul>
            </section>
        </div>
            <!-- /.sidebar -->
            <!-- dashboard Page Content -->
    <div id="page-content-wrapper">
    
   
    <div class="sound"></div>
    <div class="content-wrapper dashboardsidebar sidebarcontent" id="dashboard" style=" overflow:auto;">
			<div class="container-fluid">
			<table class="table table-bordered table-responsive">
			<thead>
			<th>Actions</th>
			<th>Event Manager Name, Email, Phone</th>
			<th>Event Name</th>
			<th>Event Date,Time,Occurance</th>
			<th>Address</th>
			<th>Latitude,Longitude</th>
			<th>Interest Category</th>
			<th>Flyer</th>
			<th>eventinterest category</th>
			</thead>
			<?php
		
		while($row = mysqli_fetch_assoc($result)) 
		{
		
		?>
			<tr>
			<td><a href="editadmindetails.php?eventId=<?php echo $row['eventId']; ?>" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			<a href="deleteEvent.php?eventId=<?php echo $row['eventId']; ?>" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
			</td>
				<td><?php echo $row["eventManagerName"].' ,'.$row["email"].' ,'.$row["eventphone"]; ?></td>
				<td><?php echo $row["eventName"]; ?></td>
				<td><?php echo $row["eventDate"].' , '.$row["eventStarttime"].' , '.$row["eventOccurance"]; ?></td>
				<td><?php echo $row["eventAddress"].' , '.$row["eventCity"].' , '.$row["eventState"].' , '.$row["zip"];  ?></td>
				<td><?php echo $row["latitude"].','. $row["longitude"]; ?></td>
				<td><?php echo $row["eventinterest"]; ?></td>
				<td><?php echo '<img class="thumbnail" height="100" src='.$row["flyer"].'>' ?></td>
				<td><?php echo $row["eventinterest"]; ?></td>
				
			</tr>
		<?php
		}
		?>
			</table>
			</div>
       
    </div>
    </div>


    <!-- Scripts -->
   <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type ="text/javascript" src="js/underscore-min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	 <script src="js/GeoCoding.js"></script>
    <script src="js/activitiesUtil.js"></script>
    <script src="js/app.min.js"></script>
<script>
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
		$("#new-activity").submit (function() {
		    $("#new-activity").validate();
		});

	</script>

    
</body>
</html>
