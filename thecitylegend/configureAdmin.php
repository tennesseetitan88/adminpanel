<?php 
	session_start();

	include_once("session_check.php");
	include_once("db.php");
	include_once("lib/lib_function.php");

	$email = '';
	$password = '';
	$confirm_password = '';
	$name = '';
	$age = '';
	$name = '';
	$keep_variable_same = TRUE;
	if(!empty($_POST['act_email'])) {
		echo "I am inside";
		$email = trim($_POST['act_email']);
		$password = trim($_POST['act_password']);
		$confirm_password = trim($_POST['act_confirm_password']);
		$name = trim($_POST['act_username']);
		$age = trim($_POST['act_age']);

		if($password !== $confirm_password) {
			display_alert('passwrd and confirm password does not match, please try again'); 
		} else {
			if (check_for_record_existence($con,$email) == TRUE) {
				display_alert($email.' already exist in the database');
			} else {
				$query = "insert into admin_info(email, password, age, user_name) values ('$email', '$password', '$age', '$name')";
				$result = mysqli_query($con,$query);
				if ($result == TRUE || mysqli_num_rows($result) > 0) {
					    $email = '';
    					$password = '';
    					$confirm_password = '';
    					$name = '';
    					$age = '';
    					$name = '';
					
					header("Location:configureAdmin.php");
					display_alert('admin is configured successfuly');
				} else {
					display_alert('admin could not be configured correctly, please try again');
				}
			}
		}
	}

?>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>TheCityLegend</title>

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Material Design Bootstrap -->
<link href="css/main.css" rel="stylesheet">

<!-- Your custom styles (optional) -->
<link href="css/dashboard.css" rel="stylesheet">



</head>

<body>

	<nav class="navbar navbar-dark green superadminnav">
        <button class="navbar-toggler hidden-sm-up" type="button"
            data-toggle="collapse" data-target="#collapseEx2">
            <i class="fa fa-bars"></i>
        </button>
        <div class="container-fluid">
            <div class="collapse navbar-toggleable-xs" id="collapseEx2">

                <a href="#" class="navbar-brand"> The City Legend</a>
                <ul class="nav navbar-nav float-xs-right">

                    <li class="nav-item alertitem" style="display: none;"><a
                        class="nav-link waves-effect waves-light btn btn-danger animated bounce infinite"><i
                            class="fa fa-envelope"></i></a></li>
                    <li class="nav-item dropdown"><a
                        class="nav-link dropdown-toggle waves-effect waves-light"
                        type="button" id="dropdownMenu1" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i
                            class="fa fa-user"></i> My Account</a>
                        <div class="dropdown-menu dropdown-primary dd-right"
                            aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn"
                            data-dropdown-out="fadeOut">
                            <a class="dropdown-item waves-effect waves-light logout" href="logout.php">Logout</a>

                        </div></li>
                </ul>
            </div>
        </div>
    </nav>

	<div id="wrapper">
     <div id="sidebar-wrapper">
     <section class="sidebar" style="">
   
     
            <ul class="sidebar-menu">
                <li class="home dashboard treeview "><a href="superadmindashboard.php">Add Activity</a></li>
                <li class="home dashboard treeview "><a href="viewadmindetails.php">Manage Activity</a></li>
                <li class="home dashboard treeview"><a href="viewuserdetails.php">View User</a></li>
                <li class="home dashboard treeview active"><a href="configureAdmin.php">Configure Admin</a></li>
                 <li class="home dashboard treeview "><a href="packagecreation.php">Create Package</a></li>
                 <li class="home dashboard treeview"><a href="viewpackagedetails.php">View Package</a></li>
            </ul>
            </section>
        </div>
            <!-- /.sidebar -->
            <!-- dashboard Page Content -->
    <div id="page-content-wrapper">
    
   
    <div class="sound"></div>
    <div class="content-wrapper dashboardsidebar sidebarcontent" id="dashboard" style=" overflow:auto;">
        <div class="container-fluid">
        <div class="col-md-12">
            <div class="card" style="margin-top: 20px;">
                <form id="admin-config" action="configureAdmin.php" method="post" >
                    <div class="card-block">
                    <div class="form-header brown darken-4">
                        <h3>Add An New admin </h3>
                    </div>
                    <div class="md-form">
                        <input type="email" id="act_email" class="form-control" name="act_email" value="<?php echo $email ?>" required />
                        <label for="act_email">Email :</label>
                    </div>
                    <div class="md-form">
                        <input type="password" id="act_password" class="form-control" name="act_password" required />
                        <label for="act_password">Password :</label>
                    </div>
                    <div class="md-form">
                        <input type="password" id="act_confirm_password" class="form-control" name="act_confirm_password" required />
                        <label for="act_confirm_password">Confirm Password :</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="act_username" class="form-control" name="act_username" value="<?php echo $name ?>" required />
                        <label for="act_username"> Admin Name :</label>
                    </div>
                    <div class="md-form">
                        <input type="number" id="act_age" class="form-control" name="act_age" required value="<?php echo $age ?>" />
                        <label for="act_age" > Age : </label>
                    </div>

                    <div class="text-xs-center">
                        <input placeholder="Submit" class="btn btn-large btn-primary" type="submit" id="act_formsubmit">

                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

	<!-- JQuery -->


	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type ="text/javascript" src="js/underscore-min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>>


	<script>
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
		$("#new-activity").submit (function() {
		    $("#new-activity").validate();
		});

	</script>
    <script src="js/activitiesUtil.js"></script>
    <script src="js/app.min.js"></script>

</body>

</html>

