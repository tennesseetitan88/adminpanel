<?php
header('Access-Control-Allow-Origin: *');
// Output JSON
$upload_dir = 'img/flyer/';

function outputJSON($msg, $status = 'error'){
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));

	echo json_encode(array( 'data' => $msg, 'status' => $status));
}

// Check for errors
if($_FILES['flyer']['error'] > 0){
    outputJSON('An error ocurred when uploading.');
}


/* Check if the file exists
if(file_exists($upload_dir . $_FILES['flyer']['name'])){
    outputJSON('File with that name already exists.');
}
*/

// Upload file
$new_image_name = urldecode($_FILES["flyer"]["name"]);
if(!move_uploaded_file($_FILES['flyer']['tmp_name'], $upload_dir.$new_image_name)){
    outputJSON('Error uploading file - check destination is writeable.');
}

// Success!
outputJSON($upload_dir . $_FILES['flyer']['name'] . '".', 'success');
?>
